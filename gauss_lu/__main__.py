from gauss_lu import gaussQt
from PyQt5.QtWidgets import QApplication
import sys

app = QApplication(sys.argv)
main_window = gaussQt.GaussQt()
sys.exit(app.exec_())