import numpy as np
import pdb
from PyQt5.QtCore import pyqtRemoveInputHook

def gauss(system):
    
    A = np.array(system, float)
    num_rows = A.shape[0]
    num_cols = A.shape[1]

    for row, col in zip(range(num_rows), range(num_cols)):
        #make the first non-zero entry of this row 1
        if A[row][col] != 0:
            A[row] = A[row] / A[row][col]

        #zero out the rest of the elements in this column under this row
        if A[row][num_cols - 1] != 0:
            bottom_rows = row + 1
            for bottom_row in range(bottom_rows, num_rows):
                row_factor = A[bottom_row][col] / A[row][col] if A[row][col] != 0.0 else 0
                for sub_col in range(col, num_cols):
                    A[bottom_row][sub_col] = A[bottom_row][sub_col] - (row_factor * A[row][sub_col])


    #initialize the x column vector
    x = [0 for i in range(num_rows)]

    for i in range(num_rows - 1, -1, -1):
        x[i] = A[i][num_cols - 1] / A[i][i] if A[i][i] != 0 else 1
        for k in range(i - 1, -1, -1):
            A[k][num_cols - 1] -= A[k][i] * x[i]

    return x


def lu(A):

    A = np.array(A, float)

    num_rows = A.shape[0]
    num_cols = A.shape[1]

    #split A into A and b
    b_vector = A[:, num_cols - 1]
    A = np.delete(A, num_cols - 1, 1)
    L = np.zeros((num_rows, num_cols), float)
    num_cols -= 1

    for row, col in zip(range(num_rows), range(num_cols)):

        #zero out the rest of the elements in this column under this row
        if A[row][num_cols - 1] != 0:
            bottom_rows = row + 1
            for bottom_row in range(bottom_rows, num_rows):
                row_factor = A[bottom_row][col] / A[row][col] if A[row][col] != 0.0 else 1
                L[bottom_row][col] = row_factor
                for sub_col in range(col, num_cols):
                    A[bottom_row][sub_col] = A[bottom_row][sub_col] - (row_factor * A[row][sub_col])

    #set the diagonal of L to 1
    for row, col in zip(range(num_rows), range(num_cols)):
        L[row][col] = 1

    #combine L and b to solve Lc = b for c
    c_vector = [0 for i in range(num_rows)]
    x = [0 for i in range(num_rows)]


    for row, col in zip(range(num_rows), range(num_cols)):
        c_vector[row] = b_vector[row] / L[row][col]
        for sub_row in range(row+1, num_rows):
            b_vector[sub_row] -= L[sub_row][col] * c_vector[row]


    #solve for Ux = c
    for i in range(num_rows - 1, -1, -1):
        x[i] = c_vector[i] / A[i][i] if A[i][i] != 0 else 1
        for k in range(i - 1, -1, -1):
            c_vector[k] -= A[k][i] * x[i]

    return x
