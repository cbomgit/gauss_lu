import sys
from gauss_lu import solvers
from PyQt5.QtWidgets import (QWidget, QGridLayout, 
    QPushButton, QLabel, QLineEdit)
from PyQt5.QtCore import Qt
import os
import pdb

class GaussQt(QWidget):

    def __init__(self):
        super().__init__()
        self.init_ui()

    def init_ui(self):

        self.grid = QGridLayout()
        self.setLayout(self.grid)

        #create the widget elements
        self.rows_input_label = QLabel('Rows')
        self.rows_input = QLineEdit()

        self.cols_input_label = QLabel('Columns')
        self.cols_input = QLineEdit()

        self.size_ok_button = QPushButton('Ok')
        self.size_ok_button.clicked.connect(self.size_confirmed)
        self.matrix_grid = QGridLayout()
        self.answer_label = QLabel('')
        self.a_label = QLabel('A')
        self.b_label = QLabel('b')

        #add the answer text label to the bottom of the layout
        self.grid.addWidget(self.answer_label, 7, 0)

        #set up the grid
        self.grid.setSpacing(10)
        self.grid.addWidget(self.rows_input_label, 1, 0)
        self.grid.addWidget(self.rows_input, 2, 0)
        self.grid.addWidget(self.cols_input_label, 1, 1)
        self.grid.addWidget(self.cols_input, 2, 1)
        self.grid.addWidget(self.size_ok_button, 3, 0)

        #initialize the number of rows and columns
        self.num_rows = 0
        self.num_cols = 0
        self.setWindowTitle('Gauss/LU Solver')
        self.move(300, 300)
        self.show()

    def get_dim_input(self):
        """
        validates that the user entered valid values for rows and columns,
        otherwise displays a message
        """

        #get the chosen matrix dimensions
        rows_input = self.rows_input.text()
        cols_input = self.cols_input.text()

        if not rows_input.isnumeric() or not cols_input.isnumeric():
            self.answer_label.setText("Please enter numbers")
            return False
        else:
            self.num_rows = int(self.rows_input.text())
            self.num_cols = int(self.cols_input.text())
            return True

    def size_confirmed(self):

        self.clear_matrix_grid()
        if self.get_dim_input():
            self.matrix_grid.setSpacing(10)

            if self.num_rows < self.num_cols - 1:
                self.answer_label.setText("Must have more equations than variables.")
            elif self.num_rows < 2:
                self.answer_label.setText("Provide at least two equations")
            else:
                #add text boxes to the grid
                self.grid.addWidget(self.a_label, 4, 0, 1, self.num_cols - 2, Qt.AlignLeft)
                self.grid.addWidget(self.b_label, 4, self.num_cols - 2, 1, 1, Qt.AlignRight)
                for row in range(self.num_rows):
                    for col in range(self.num_cols):
                        text_box = QLineEdit()
                        self.matrix_grid.addWidget(text_box, row, col)

                #add the matrix grid to the main grid layout
                self.grid.addLayout(self.matrix_grid, 5, 0, 1, 2)

                #set up the gauss and LU solver buttons
                self.gauss_solver_button = QPushButton('Solve With Gauss')
                self.gauss_solver_button.clicked.connect(self.solve_with_gauss)

                self.lu_solver_button = QPushButton('Solve with LU')
                self.lu_solver_button.clicked.connect(self.solve_with_lu)

                self.grid.addWidget(self.gauss_solver_button, 6, 0)
                self.grid.addWidget(self.lu_solver_button, 6, 1)

    def clear_matrix_grid(self):
        self.answer_label.setText("")
        for pos in reversed(range(self.matrix_grid.count())):
            widget_to_remove = self.matrix_grid.itemAt(pos).widget()
            self.matrix_grid.removeWidget(widget_to_remove)
            widget_to_remove.setParent(None)

        #also remove labels for A and b
        if self.grid.itemAtPosition(4, 0) is not None:
            self.a_label = self.grid.itemAtPosition(4, 0).widget()
            self.grid.removeWidget(self.a_label)
            self.a_label.setParent(None)

        if self.grid.itemAtPosition(4, self.num_cols - 2) is not None:
            self.b_label = self.grid.itemAtPosition(4, self.num_cols - 2).widget()
            self.grid.removeWidget(self.b_label)
            self.b_label.setParent(None)

    def get_input_matrix(self):
        matrix = []
        for row in range(self.num_rows): 
            matrix.append([])
            for col in range(self.num_cols): 
                text = self.matrix_grid.itemAtPosition(row, col).widget().text()
                matrix[row].append(int(text))

        return matrix

    def set_answer_label(self, answer):
        solution_string = ""
        if answer != 'No solution':
            for ndx,val in enumerate(answer):
                solution_string += "x"+str(ndx)+" = " + str(val) + os.linesep 
        else:
            solution_string = answer

        self.answer_label.setText(solution_string)

    def solve_with_gauss(self):
        matrix = self.get_input_matrix()
        ans = solvers.gauss(matrix)
        self.set_answer_label(ans)

    def solve_with_lu(self):
        matrix = self.get_input_matrix()
        ans = solvers.lu(matrix)
        self.set_answer_label(ans)
