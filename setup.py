from setuptools import setup

setup(
      name='gauss_lu',
      description='Gauss and LU factorization module',
      url='https://bitbucket.org/cbomgit/gauss_lu',
      author='Christian Boman',
      author_email='christianbboman@gmail.com',
      license='MIT',
      packages=['gauss_lu'],
      include_package_data=True,
      install_requires = [
        'numpy',
        'pyqt5' 
      ],
      zip_safe=False
)
